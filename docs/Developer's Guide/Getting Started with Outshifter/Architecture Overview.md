# Architecture Overview

Outshifter consists of three distinctive components:

First is the Outshifter Core which is the backend server that exposes the GraphQL API. The core is written in Python and does not have a user interface of its own. It maintains its state in a PostgreSQL database and caches some information in Redis where available.

Then there's Outshifter Dashboard which implements the user interface that staff members can use to operate a store. The dashboard is a React application that runs in the browser and talks to the core server. It's a static website so it does not have any backend code of its own.

Lastly there's Outshifter Storefront which is an example storefront implemented in React. You can customize its code to suit your needs or you can build a custom storefront using the underlying Outshifter SDK.

All three components communicate using GraphQL over HTTPS.