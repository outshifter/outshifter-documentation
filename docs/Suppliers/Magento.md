---
sidebar_position: 4
---

# Magento

## Installation

In order to export your Magento store products to Outshifter, you need to install the Outshifter for Suppliers plugin first of all. To do this:

· Open your the console, once the console is open, you must locate the folder where you have installed your Magento. 

· Being in the folder you must activate the production mode using the following command.

`deploy:mode:set production`

· Let the process finish until the console indicates that the production mode is enabled by the following message: 

`Enabled production mode`

· Once you have the production mode activated, the next step is to install the plugin in our Magento marketplace, for this we must use the following commands:

```
(You have to use the following commands one by one for the correct installation.)

composer require outshifter/connector:1.0.0
php bin/magento module:enable Outshifter_Outshifter
php bin/magento setup:upgrade
php bin/magento setup:di:compile
php bin/magento setup:static-content:deploy
php bin/magento cache:flush
```
· After applying the first command you will be asked for the **Access Keys** of your Magento account. To find them you will have to: 

· Log into your Magento account (https://marketplace.magento.com), click on the dropdown of your user and choose the **My Profile** option.

![Docu](/img/signindropdown.png)

There you will look for the Access Keys section. 

![Docu](/img/accesskeysmenu.png)

· Within this you look for the button ![Docusaurus logo](/img/createkey.png) and give a name to the key that you are going to use 

· Click on the **Ok** button. Two keys will appear, the **Public Key** is the **Username** and the **Private Key** is the **Password**. You have to paste them on the console when the authentication is required.

![Docu](/img/authentication.png)

When you have entered the access keys you should continue with the other commands until the installation is complete.

## Connection

Once the plugin is installed, the next step is to authorize the app to use the Magento rest API and save the Outshifter API Key.To save the Outshifter API Key 

· Go to the **Stores** section located on the left of the screen. Inside Stores look for the **Configuration** section 

· Inside configuration look for Outshifter and click, it will open two fields where you must add the API Key of your Outshifter user and the url of the Outshifter API (https://api.outshifter.com).
The Outshifter API Key can be obtained by logging into Outshifter (https://app.outshifter.com/login)

· Look for the button ![Docusaurus logo](/img/editprofile.png), click, it will take you to your profile

· Look for the **Sales Channels** section, there you will see a section called **API Key**, if you don't have a key created click on the **Create** button, in case you already have it created click on the **Copy** button. That key must be pasted in the General section in Magento.


![Docusaurus logo](/img/outshiftersection.png)

To authorize the integration, you have to go to **System** on the left side of the screen and then look for the **Integrations** section.In Integrations you will find an integration called Outshifter, click on Activate, another tab will open where you will have to log in to your Outshifter Suppliers account, after logging in, click on Connect. With that the user is already connected. After connecting our Outshifter Supplier, you should go to the **Catalog** section and within this click on Products. There you will see all your products, select all those you want to export to Outshifter, select all of them click on the dropdown called **Actions** and select the option called **Send to Outshifter**. This will export all the selected products to Outshifter.


## Listings

**Important note:** to be able to publish products on Outshifter, you must first receive verification from Outshifter, which will authorize you to publish products on the platform. Now that your Shopify products are exported, to publish them:

· Go to the ![Docusaurus logo](/img/3.png) tab in your Outshifter account and go to the Listings page. 

· Click on the ![Docusaurus logo](/img/5.png) button, on the Drafts tab you will find  all the products you have exported.

· Edit the fields that are needed to be able to publish a product. 

· Choose the product you want to publish and fill in the necessary fields, when you have finished click the button ![Docusaurus logo](/img/6.png), when the changes have been saved click the button ![Docusaurus logo](/img/7.png). 

· Once the product is published, it will be available to all sellers who want to export it to their online store.

## Manage Orders

The management of shopify orders is carried out from the orders tab that you will find in the Outshifter platform. The management of shopify orders is carried out from the orders tab that you will find in the Outshifter platform ![Docusaurus logo](/img/8.png). 

· There you will see all the orders that have been made with the products that you have synchronized from your Shopify store.In the orders tab you will find all your orders, each one with its order number, the date the order was made, the total price and the order status. 

· You will also find a button called filters that will allow you to filter your search and thus obtain a more exact result when searching for a specific order. 

· You will find 3 types of filters, the status filter that will allow you to search the order depending on the state it is in, a platform filter will allow you to filter the orders so that only the orders of the desired platform are shown and a filter of date to search for orders between certain dates.

![Docusaurus logo](/img/9.png)

· You will also receive a notification in the Feed tab, each time a new order arrives. The number of the order will appear and clicking the button called view details, it will redirect you to the order.

![Docusaurus logo](/img/29.png)

· There you will find all the information, both of the purchased product, such as the customer's information and the final price summary including: the payment method , product price, shipping costs, service fee, commission that the seller will receive and the total revenue.

## Fullfil

The process of completing the orders is as follows, you must locate the order you need to complete, you can do it from the **Orders** tab in Otshifter or from the **Feed**. Once you are in the order:

· Add the tracking number of the order by clicking on ![Docusaurus logo](/img/12.png) the button. A modal will open where you can add your tracking number and select the company that will deliver the product.

![Docusaurus logo](/img/13.png)