# Prices

## Introduction

Outshifter API has four different types that represent prices. The most basic type consists of a currency code and a numeric amount. The most complex one can represent a range of prices including net, gross, and tax amounts.

Outshifter does not enforce any particular set of currency codes but we strongly recommend using the three-letter codes defined by the ISO 4217 standard.

## Displaying prices

How a number is presented depends on the language and region. If you are building a web application, we recommend that you use the ``Intl.NumberFormat`` API.

## API Reference

### Money

``Money`` is a basic type for keeping prices without taxes. We do not restrict currency code values, so a user use crypto-currencies or other custom currency.

· ``currency``: Currency code for price. Currency depends on chosen Channel.

· ``amount``: Price amount.

### TaxedMoney

The most common example of TaxedMoney is a ProductVariant price. If a user does not use any tax integration, both net and gross prices will be populated with the same value.

· ``currency``: Currency code for price. Currency depends on chosen Channel.

· ``gross``: Gross value (price with taxes).

· ``net``: Net value (price without taxes).

· ``tax``: Tax value.

### MoneyRange

MoneyRange is used to display prices for a ShippingZone.

start: Lowest price in the range.

stop: Highest price in the range.

### TaxedMoneyRange

TaxedMoneyRange is used in Products, because variants can differ in prices. You can display it as "price starts from $10" or "$10–$20".

· ``start``: Lowest price in the range.

· ``stop``: Highest price in the range.