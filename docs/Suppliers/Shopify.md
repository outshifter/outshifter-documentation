---
sidebar_position: 1
---

# Shopify

## Installation

In order to export your Shopify store products to Outshifter, you need to install the Outshifter for Suppliers app first of all. 

· Go to your Shopify store, there you should look for the  section ![Docusaurus logo](/img/apps.png)

· Click on it and it will take you to the section where you can install all the applications for your Shopify store. 

· Look for a button called ![Docusaurus logo](/img/customize.png). 

· Click on it and it will take you to the Shopify App Store, there you must search for the Outshifter for Suppliers app which will allow you to export your products saved in your Shopify store to Outshifter.When you have found it

· Click on the ![Docusaurus logo](/img/addapp.png) button, this will redirect you to your Shopify store and ask you to confirm the installation of the app. 

· Click the Install app button, the app will be installed and will ask you to log into your Outshifter account in order to export your products.

![Docusaurus logo](/img/shopicon.png)
## Connection

Once the Outshifter for Suppliers app is installed, you will be shown a window where it will ask for the credentials of your Outshifter account.

![Docusaurus logo](/img/singup.png)

After you have logged in, you will receive a message on the screen indicating that the connection of your accounts was successful.

![Docusaurus logo](/img/1.png)

· You will be shown a button called Sync Products, click on it and all the products in your Shopify account will begin to sync. Wait for the app to finish syncing the products. 

· Once the sync has finished, the products will be available to be published in your Outshifter account. From the same Outshifter app, the supplier is given the possibility to desynchronize those products that they do not want to have in Outshifter. In case of adding new products to the Shopify store, the method to synchronize them with the Outshifter account is simple, you must open the app and select the products you want to synchronize and once selected click on ![Docusaurus logo](/img/2.png).

## Listings

**Important note:** to be able to publish products on Outshifter, you must first receive verification from Outshifter, which will authorize you to publish products on the platform. Now that your Shopify products are exported, to publish them:

· Go to the ![Docusaurus logo](/img/3.png) tab in your Outshifter account and go to the Listings page. 

· Click on the ![Docusaurus logo](/img/5.png) button, on the Drafts tab you will find  all the products you have exported.

· Edit the fields that are needed to be able to publish a product. 

· Choose the product you want to publish and fill in the necessary fields, when you have finished click the button ![Docusaurus logo](/img/6.png), when the changes have been saved click the button ![Docusaurus logo](/img/7.png). 

· Once the product is published, it will be available to all sellers who want to export it to their online store.

## Manage Orders

The management of shopify orders is carried out from the orders tab that you will find in the Outshifter platform. The management of shopify orders is carried out from the orders tab that you will find in the Outshifter platform ![Docusaurus logo](/img/8.png). 

· There you will see all the orders that have been made with the products that you have synchronized from your Shopify store.In the orders tab you will find all your orders, each one with its order number, the date the order was made, the total price and the order status. 

· You will also find a button called filters that will allow you to filter your search and thus obtain a more exact result when searching for a specific order. 

· You will find 3 types of filters, the status filter that will allow you to search the order depending on the state it is in, a platform filter will allow you to filter the orders so that only the orders of the desired platform are shown and a filter of date to search for orders between certain dates.

![Docusaurus logo](/img/9.png)

· You will also receive a notification in the Feed tab, each time a new order arrives. The number of the order will appear and clicking the button called view details, it will redirect you to the order.

![Docusaurus logo](/img/29.png)

· There you will find all the information, both of the purchased product, such as the customer's information and the final price summary including: the payment method , product price, shipping costs, service fee, commission that the seller will receive and the total revenue.

## Fullfil

The process of completing the orders is as follows, you must locate the order you need to complete, you can do it from the **Orders** tab in Otshifter or from the **Feed**. Once you are in the order:

· Add the tracking number of the order by clicking on ![Docusaurus logo](/img/12.png) the button. A modal will open where you can add your tracking number and select the company that will deliver the product.

![Docusaurus logo](/img/13.png)