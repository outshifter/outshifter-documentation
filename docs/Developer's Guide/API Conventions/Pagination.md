# Pagination

Pagination is required in most queries which return lists of items in the Saleor GraphQL API. This limits the number of results returned by the server to a more manageable size and avoids data flow disruptions.

Outshifter uses cursor-based pagination which means that instead of using page numbers, each item is assigned a unique cursor and API clients can ask for a certain number of items following or preceding a given item.

There are two types of lists in GraphQL:

- ``[Foo]`` is a simple list. It is used to query a list containing several items.

  A good example of a simple list could be a query for product variants which returns a list with a manageable number of results.

- ``FooConnection`` represents a more complex list. When queried, it will return an unknown or large number of results.

Pagination is used to help you handle large amounts of items returned by the connection list type.

Outshifter's pagination model is based on the GraphQL Connection Specification but contains some extra fields added for your convenience, its schema looks like this:

```json
type FooConnection {
  pageInfo: PageInfo!
  edges: [FooEdge!]!
}

type PageInfo {
  hasNextPage: Boolean!
  hasPreviousPage: Boolean!
  startCursor: String
  endCursor: String
}

type FooEdge {
  node: Foo!
  cursor: String!
}

```

You can use the ``PageInfo`` object to know whether a previous or next page exists (``hasPreviousPage``, ``hasNextPage``) and how to reach them (``startCursor``, ``endCursor``). 

You can also use individual ``cursor`` values returned with each node but it's usually less convenient.

## Querying a connection

To retrieve a large number of results and indicate how you want the queried items to be fetched, use a combination of the following connection arguments:

- ``first``: Followed by the number of items (nodes) you want the query to return. This argument will return items starting from the beginning of the list.

```json
query {
  products(first: 10) {
    edges {
      cursor
      node {
        id
        name
      }
    }
  }
}
```

- ``last``: Followed by the number of items (nodes) you want the query to return. This argument will return items starting from the end of the list.

```json
query {
  products(last: 10) {
    edges {
      cursor
      node {
        id
        name
      }
    }
  }
}
```

- ``after``: Followed by the value of the cursor. This indicates the direction in which you want to start displaying items; in this case, it is from the beginning of the list.

```json
query {
  products(first: 10, after: "YXJyYXljb25uZWN0aW9uOjA=") {
    edges {
      node {
        id
        name
      }
    }
  }
}
```

- ``before``: Followed by the value of the cursor. This indicates the direction in which you want to start displaying items; in this case, it is from the end of the list.

```json
query {
  products(last: 10, before: "YXJyYXljb25uZWN0aW9uOjQ==") {
    edges {
      node {
        id
        name
      }
    }
  }
}
```


## Connection query results

Each connection can return the following fields:

- ``edge``: Nodes are connected to each other by the edges. An edge contains the following fields:

  - ``node``: Represents an object you queried for, an item on the list (for example, a product).

  - ``cursor``: The value of the cursor representing the place where the node is located.

- ``pageInfo``: Consists of information on the current connection.

  - ``hasNextPage``: Indicates if there are more records to be displayed (hence, if there is a next page). This is a Boolean value.

  - ``hasPreviousPage``: Indicates if there are records to be displayed prior to the current page (hence, if there is a previous page). This is a Boolean value.

  - ``startCursor``: Indicates the cursor value of the first item on the page.

  - ``endCursor``: Indicates the cursor value of the last item on the page.

```c
query {
  products(first: 10) {
    edges {
      cursor
      node {
        id
        name
      }
    }
    pageInfo {
      hasNextPage
      hasPreviousPage
      startCursor
      endCursor
    }
  }
}
```  

For more information about pagination, see the official GraphQL website.



