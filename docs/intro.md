---
sidebar_position: 1
---

# Integrations
# Welcome to Outshifter Documentation

Here you'll find everything you need to start working with **Outshifter**.

## Suppliers

· [**Shopify**](https://outshifter-documentation.netlify.app/docs/Suppliers/Shopify) all you need to know about the Shopify integration.

· [**WooCommerce**](https://outshifter-documentation.netlify.app/docs/Suppliers/WooCommerce) all you need to know about the WooCommerce integration.

· [**Squarespace**](https://outshifter-documentation.netlify.app/docs/Suppliers/Squarespce) all you need to know about the Squarespce integration.

· [**Magento**](https://outshifter-documentation.netlify.app/docs/Suppliers/Magento) all you need to know about the Magento integration.

## Sales channels

· [**Outshifter Embed Commerce**](https://outshifter-documentation.netlify.app/docs/Sales%20Channels/OutshifterBlocks) all the information about the Outshifter Blocks.

