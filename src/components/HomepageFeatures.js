import React from 'react';
import clsx from 'clsx';
import styles from './HomepageFeatures.module.css';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faDotCircle} from "@fortawesome/free-regular-svg-icons";

const FeatureList = [
  {
    title: 'Getting Started',
    description: (
      <>
        Welcome to Docs UI Kit! We're so glad you're here. Let's get started!
      </>
    ),
  },
  {
    title: 'Account',
    description: (
      <>
        Adjust your profile and preferences to make Docs UI Kit work just for you!
      </>
    ),
  },
  {
    title: 'Subscription',
    description: (
      <>
        Assistance on how and when you might use the subscription product.
      </>
    ),
  },
  {
    title: 'Tips, tricks & more',
    description: (
      <>
        Tips and tools for beginners and experts alike.
      </>
    ),
  },
];

const colors = ["blue","red","green","orange"];

function Feature({title, description, index}) {
  return (
    <div className={clsx('col col--6')}>
    <div className={styles.feature}>
    <FontAwesomeIcon style={{ color: colors[index]}} icon={faDotCircle}/>
    <a href="https://outshifter-documentation.netlify.app/"> {title}</a>
    <p>{description}</p>
    </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
      <div className="row">
        <div className="col"> 
        <h2>Knowledge Base</h2>
        <p>Htmlstream community is your first stop for questions and advice about the UI Kit. Welcome to the community!</p>
        </div>
      </div>
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} index={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
