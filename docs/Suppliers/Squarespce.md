---
sidebar_position: 3
---

# Squarespace

## Installation

In order to use Outshifter and Squarespace services, you do not need any type of installation, the only requirements you need are: **a supplier account in Outshifter** and a **Squarespace store** with the products you want to export to the Outshifter platform.

## Connection

In order to connect your store with Outshifter, you have to obtain a Squarespace **Developer Key** that will later be used on the Outshifter platform to connect the Squarespace store with your Outshifter Supplier account. To do this 

· Go to your main page of your Squarespace store

· Open the **Settings** section in the sidebar on the left.

![Docu](/img/sidebar.png)

· Within settings we will look for the section called **Advanced**

· Open it and more options will appear within Advanced.

![Docu](/img/settingsbar.png)

· Select the option called **Developer API Key**, a button will appear to generate a new key. ![Docu](/img/generatekey.png)

![Docu](/img/advanced.png)

 · A modal will open where it will ask you for a name to identify your key, and a series of permissions that you must grant in order for the export of products to Outshifter to be carried out correctly.

![Docu](/img/permissions1.png) ![Docu](/img/permissions2.png)

· To finish we generate the key, and a new window will appear with the key that you must copy and paste in your Outshifter profile.

![Docu](/img/copykey.png)

· Head over to and log in with your Outshifter account. 

· Enter your edit profile and look for the section called ![Docu](/img/ecommercesystem.png)

· In the dropdown select the Sqaurespace option and paste the Key that you copied previously and click on connect, your products will begin to export, when it finishes it will indicate that the export has been carried out correctly.

![Docu](/img/keypastesquare.png)

## Listings

**Important note:** to be able to publish products on Outshifter, you must first receive verification from Outshifter, which will authorize you to publish products on the platform. Now that your Shopify products are exported, to publish them:

· Go to the ![Docusaurus logo](/img/3.png) tab in your Outshifter account and go to the Listings page. 

· Click on the ![Docusaurus logo](/img/5.png) button, on the Drafts tab you will find  all the products you have exported.

· Edit the fields that are needed to be able to publish a product. 

· Choose the product you want to publish and fill in the necessary fields, when you have finished click the button ![Docusaurus logo](/img/6.png), when the changes have been saved click the button ![Docusaurus logo](/img/7.png). 

· Once the product is published, it will be available to all sellers who want to export it to their online store.

## Manage Orders

The management of shopify orders is carried out from the orders tab that you will find in the Outshifter platform. The management of shopify orders is carried out from the orders tab that you will find in the Outshifter platform ![Docusaurus logo](/img/8.png). 

· There you will see all the orders that have been made with the products that you have synchronized from your Shopify store.In the orders tab you will find all your orders, each one with its order number, the date the order was made, the total price and the order status. 

· You will also find a button called filters that will allow you to filter your search and thus obtain a more exact result when searching for a specific order. 

· You will find 3 types of filters, the status filter that will allow you to search the order depending on the state it is in, a platform filter will allow you to filter the orders so that only the orders of the desired platform are shown and a filter of date to search for orders between certain dates.

![Docusaurus logo](/img/9.png)

· You will also receive a notification in the Feed tab, each time a new order arrives. The number of the order will appear and clicking the button called view details, it will redirect you to the order.

![Docusaurus logo](/img/29.png)

· There you will find all the information, both of the purchased product, such as the customer's information and the final price summary including: the payment method , product price, shipping costs, service fee, commission that the seller will receive and the total revenue.

## Fullfil

The process of completing the orders is as follows, you must locate the order you need to complete, you can do it from the **Orders** tab in Otshifter or from the **Feed**. Once you are in the order:

· Add the tracking number of the order by clicking on ![Docusaurus logo](/img/12.png) the button. A modal will open where you can add your tracking number and select the company that will deliver the product.

![Docusaurus logo](/img/13.png)