# Extensibility

The obvious way to extend Outshifter would be to download the source code and modify it directly.

After all it's an open-source platform. We advise against this as experience teaches us that once your store diverges from the upstream Outshifter, it becomes hard to keep it updated.

Because of this we propose two ways to add functionality to Outshifter:

· **Plugins** offer a way to run additional code as part of Outshifter Core. They have full access to the database.

· **Apps** are external applications that talk to Saleor Core using its GraphQL API and can subscribe to events using webhooks.

To learn more, see Extending Outshifter.