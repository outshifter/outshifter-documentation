---
sidebar_position: 2
---

# WooCommerce

## Installation

In order to export your WooCommerce store products to Outshifter, you need to install the Outshifter plugin first of all. There are two different ways to find and install the plugin. 

· The first one is through the WordPress plugin search engine. To find it you must go to the url of your WooCommerce, once there you must look for the section called Plugins on the left side of your page.

![Docusaurus logo](/img/14.png)

· Click on Plugins, this will take you to the page where you will find all the plugins you have installed.

· Look for the button ![Docusaurus logo](/img/19.png), it will take you to the WordPress plugin search engine. 

·In the plugin search tab you will write “outshifter” and the Outshifter plugin will appear. 

· Click Install Now and wait for the plugin to install correctly, once it has been installed you must activate it

· Click the Active button and let the plugin activate, once activated it will be ready to be connected with Outshifter.

![Docusaurus logo](/img/15.png) ![Docusaurus logo](/img/16.png)
     

The other way to install the plugin is by downloading it from the Wordpress website. 

· Go to https://wordpress.org/plugins/outshifter-export/. Look for the button ![Docusaurus logo](/img/18.png)  and download the plugin. 

· Once the plugin is downloaded, the next step is to go back to the Plugins section in your WordPress. Then look for the button![Docusaurus logo](/img/19.png)

· Click on it and now look for the button ![Docusaurus logo](/img/20.png) and click on it. A section will open where you can load the plugin that you have previously downloaded. 

· Click on ![Docusaurus logo](/img/21.png) and browse for the Outshifter plugin file.

· When you have selected it and it is loaded, click on ![Docusaurus logo](/img/22.png) and wait for the plugin to install correctly. When it has been installed it will ask you to activate the plugin, click on the activate button. When it has been activated you are ready to connect with Outshifter.

## Connection

With the Outshifter plugin already installed, the next step is to connect your Outshifter supplier account with the plugin. To do this you must go to the WooCommerce plugin in WordPress,

![Docusaurus logo](/img/23.png)

there you must enter Settings, within Settings you will find a tab called ![Docusaurus logo](/img/24.png), click on it and you will see that a form appears in which you will have to log in with your Outshifter supplier account.

![Docusaurus logo](/img/25.png)

Once you are logged in, a window will appear where you will be asked to choose the currency you want to use, choose the type of currency and click on the save button to save the configuration. Once the configuration is saved, you click the continue button.

![Docusaurus logo](/img/26.png)

## Listings

In order to sync your products is through the Products section of WordPress. 

· Go to it, once there, look for the Bulk actions dropdown,![Docusaurus logo](/img/27.png) click on it and select the Outshifter Sync option

· Select the products you want to synchronize and click on the ![Docusaurus logo](/img/28.png) button. 

· Wait for the product synchronization bar to finish and your products are ready to be published on your Outshifter Listings page.

**Important note:** To be able to publish products on Outshifter, you must first receive verification from Outshifter, which will authorize you to publish products on the platform.

Now that your WooCommerce products are exported, to publish them: 

· Go to the Products tab in your Outshifter account and go to the Listings page. Once there, all the products you have exported will appear.

· Edit the fields that are needed to be able to publish a product. 

· Choose the product you want to publish and fill in the necessary fields, when you have finished click the button ![Docusaurus logo](/img/6.png), when the changes have been saved click the button ![Docusaurus logo](/img/7.png). 

· Once the product is published, it will be available to all sellers who want to export it to their online store.

## Manage Orders

The management of shopify orders is carried out from the orders tab that you will find in the Outshifter platform. The management of shopify orders is carried out from the orders tab that you will find in the Outshifter platform ![Docusaurus logo](/img/8.png). 

· There you will see all the orders that have been made with the products that you have synchronized from your Shopify store.In the orders tab you will find all your orders, each one with its order number, the date the order was made, the total price and the order status. 

· You will also find a button called filters that will allow you to filter your search and thus obtain a more exact result when searching for a specific order. 

· You will find 3 types of filters, the status filter that will allow you to search the order depending on the state it is in, a platform filter will allow you to filter the orders so that only the orders of the desired platform are shown and a filter of date to search for orders between certain dates.

![Docusaurus logo](/img/9.png)

· You will also receive a notification in the Feed tab, each time a new order arrives. The number of the order will appear and clicking the button called view details, it will redirect you to the order.

![Docusaurus logo](/img/11.png)

· There you will find all the information, both of the purchased product, such as the customer's information and the final price summary including: the payment method , product price, shipping costs, service fee, commission that the seller will receive and the total revenue.

## Fullfil

The process of completing the orders is as follows, you must locate the order you need to complete, you can do it from the **Orders** tab in Otshifter or from the **Feed**. Once you are in the order:

· Add the tracking number of the order by clicking on ![Docusaurus logo](/img/12.png) the button. A modal will open where you can add your tracking number and select the company that will deliver the product.

![Docusaurus logo](/img/13.png)