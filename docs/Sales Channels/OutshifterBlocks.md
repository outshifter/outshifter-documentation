---
sidebar_position: 1
---

# Outshifter Embed Commerce

## Installation

In order to post your products from Outshifter in a site or page on your Wordpress, you need to install the Outshifter Blocks plugin first of all. There are two different ways to find and install the plugin. The first one is through the WordPress plugin search engine. To find it you must go to the url of your Wordpress, once there you must look for the section called Plugins on the left side of your page.

![Docusaurus logo](/img/14.png)

Click on Plugins, this will take you to the page where you will find all the plugins you have installed. Now you should look for the button ![Docusaurus logo](/img/19.png), it will take you to the WordPress plugin search engine. In the plugin search tab you will write “Outshifter Embeed Commerce” and the Outshifter Blocks plugin will appear. Click Install Now and wait for the plugin to install correctly, once it has been installed you must activate it, click the Active button and let the plugin activate, once activated it will be ready to be connected with Outshifter.

## Configuration

Once the plugin is installed and activated, you have to connect it with your Outshifter account using the Api Key. Look for the section called "Outshifter blocks" in the left sidebar.

![Image](/img/sidebarblocks.png)

Once inside, it will ask you for the Api Key to be able to connect the account correctly.

![Image](/img/apikeyconnect.png)

This key can be obtained from your Outshifter account profile. Go to https://app.outshifter.com/login, login and go to ![Image](/img/editprofile.png), look for the section called "Sales Channels". The API Key will appear with a Copy button, in case you do not have the key you will have to create it, click on Create and copy and paste it in the Outshifter Blocks plugin.

Once the connection has been made, it is time to configure the different parameters of the plugin. You will find 4 sections, Currency where you must choose the currency you want to use.

![Image](/img/currency.png)

The 2nd section is Logo, where you must add the logos you want to use.

![Image](/img/brandlogos.png)

In the Styles section, you can choose the color you want for your Layout in your WordPress posts and pages.

![Image](/img/colorlayout.png)

Finally in Layout you can choose the style of the Priduct Card and the alignment of the text of the Product Card. You also have the possibility to choose where you want the product purchase method to appear.

![Image](/img/stylelayout.png)

## Blocks

Within the previously installed Outshifter plugin you can find 6 different types of blocks that you can use both for your posts and for your Wordpress pages. The process to add them is very simple, you will go to pages or posts depending on your needs.

![Image](/img/sidebarpp.png)

Once there you will choose a new post / page or in the case of wanting to use one already created, you will choose the one created, the process is the same. By means of the button ![Image](/img/addblock.png) the sidebar will appear to be able to choose the blocks, look for the section where the Outshifter blocks are and you will be able to add the one you want the most.

![Image](/img/blocksmenu.png)

### Banner Block

The **Banner block** is simple and allows you to add a banner to your posts or pages. Add it and it will appear to be able to choose the image you want to use, select the image and save the changes.

![Image](/img/banner.png)

### Single Product

**Single Product** block allows you to select one of your products and be able to add it to your posts or pages, giving it the possibility of making the purchase to any user who wishes to buy the product. Add it and a button called choose the product will appear, select the desired product and it will be automatically added to the block.

![Image](/img/singleproduct.png)

### Two Products

**Two Products** block has the same function as the **Single Product** block but instead of one product you will have the possibility to add two products. Choose the two desired products and they will be added to your block.

![Image](/img/twoproducts.png)

### Carousel

The **Carousel** block is different from the previous two, first because it offers you the possibility to add 5 products at the same time in the same block, and second because it offers the user to navigate between the products using two buttons that will allow you to go from right to left and to the left. reverse. To add the products click on Change Products and save the changes

![Image](/img/carousel.png)

### Shop

**Shop block** is the block that gives you the possibility to add more products up to 14 in the same block. You also have the option of adding 4 or 7 products in case of lesser need. To add the products, click on changes products, choose the desired products and save the changes.

![Image](/img/masonry.png)

### Blocks Costumization

Each of the blocks offers you the possibility to customize it. The customization is divided into 3 sections, Title, Settings and Product Card Apparence.

· Title section it allows you to put a name with which you can identify the block

![Image](/img/blocktitle.png)

· In the Settings section You will find the option that allows you to;  

Choose the type of layout you want for your post or wordpress page, there is to diferent variations of layout, Masonry and Shop.

![Image](/img/layoutvariation.png)

You can decide the amount of products you want to have in the block, between 4, 7 or 14.

![Image](/img/productquantity.png)

And you will have the possibility of change the background color of the layout.

![Image](/img/backgroundcolor.png)

· In the Product Card Apparence section You will have different customization sections.

The first one will allow you to choose the aspect ratio that you want the product card to have on your block, there are 5 different styles.

![Image](/img/productcardstyle1.png)

The second allows you to choose the thickness of the border of the card.

![Image](/img/productcardstyle2.png)

As a third option you will find the option to choose the alignment of the text in the product card.

![Image](/img/productcardstyle3.png)

And finally in Product Card Content you can choose what is shown in the product card, Product Title, Price, Supplier of that product and finally a button to make a purchase of that product

![Image](/img/productcardstyle4.png)

