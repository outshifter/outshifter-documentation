# Translations

## Introduction

Translating shop consist of two steps:

· Translating storefront application.

· Translating products and other data available in API.

Frontend part can be managed using extensions like Format.JS. Generated files can be later edited by hand or using external service, for example Transifex or POEditor. Besides text, also prices and dates should be localized using ``Intl.NumberFormat`` and date-fns.

For translating database data Saleor delivers specialized translation objects which can be modified in dashboard or via API.

## Objects available for translation

· ``ProductTranslation``

·``CollectionTranslation``

· ``CategoryTranslation``

· ``AttributeTranslation``

· ``AttributeValueTranslation``

· ``ProductVariantTranslation``

· ``PageTranslation``

· ``ShippingMethodTranslation``

· ``SaleTranslation``

· ``VoucherTranslation``

· ``MenuItemTranslation``

## Permissions

All users who have a ``MANAGE_TRANSLATIONS`` permission can create and modify translations. Additional permissions for managing objects are not required.

## How to add translation

As a example we will use ``categoryTranslate`` mutation. Fields omitted in parameter ``input`` will not be overwritten, so you can choose to only translate one field at a time:

## How to query translation

Let's query the API for a previously created category translation. A `translation` field in the Category model has a ``languageCode`` parameter which requires LanguageCodeEnum.

Fields which were not yet translated will be returned as `null` values.